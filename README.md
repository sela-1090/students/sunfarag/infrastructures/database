# PostgreSQL installation guide with Helm



## Getting started



```
kubectl delete pvc --namespace [namespace] [pvc_name]
```

To install Postgresql using helm, run the following commands:

```
helm repo add bitnami https://charts.bitnami.com/bitnami

helm repo update

helm install my-postgresql bitnami/postgresql --version 12.7.1

```
To get the "postgres" user's password, run the following command (Change the values in [] brackets to match your environment):

```
kubectl get secret --namespace [namespace name] [service name] -o jsonpath="{.data.postgres-password}"

```

connect to the server and run the psql command
```
kubectl exec -it [server pod name] --namespace [namespace name] -- /opt/bitnami/scripts/postgresql/entrypoint.sh /bin/bash

psql 

```

## To use custom values in the deployment of PostgreSQL:
1. Create a values.yaml file
2. insert the desired values into the file
3. run the following command:
```
helm install postgresql -f values.yaml bitnami/postgresql --namespace [namespace]

```
